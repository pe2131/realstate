﻿using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Service.RepositoryService.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryService.Repositories
{
    public class HouseRepository : IHouseRepository
    {
        private readonly ApplicationDbContext _Db;

        public HouseRepository(ApplicationDbContext db)
        {
            _Db = db;
        }

        public async Task DeleteHouse(Tb_House Model)
        {
            if (HoseExists(Model.Id))
            {
                Model.IsDeleted = true;
                await UpdateHouse(Model);
            }
        }
        public async Task<Tb_House> GetHouseById(int Id)
        {
            return await _Db.tb_Houses.Where(q => q.Id==Id).Include(q=>q.Tb_Owner).FirstOrDefaultAsync();
        }
        public async Task DeleteHouse(int Id)
        {
            if (HoseExists(Id))
            {
                var House = await GetHouseById(Id);
                House.IsDeleted = true;
                await UpdateHouse(House);

            }
        }

        public async Task<List<Tb_House>> GetAllHouses()
        {
            return await _Db.tb_Houses.Include(c => c.Tb_Owner).Where(c => c.IsDeleted == false).ToListAsync().ConfigureAwait(false);
        }

        public async Task<List<Tb_House>> GetDeletedHouses()
        {
            return await _Db.tb_Houses.Include(c => c.Tb_Owner).Where(c => c.IsDeleted == true).ToListAsync().ConfigureAwait(false);
        }

        public bool HoseExists(int Id)
        {
            return _Db.tb_Houses.Any(e => e.Id == Id);
        }

        public async Task InsertHouse(Tb_House Model)
        {
            await _Db.tb_Houses.AddAsync(Model);
            await _Db.SaveChangesAsync();
        }

        public async Task UpdateHouse(Tb_House Model)
        {
            _Db.Entry(Model).State = EntityState.Modified;
            await _Db.SaveChangesAsync();
        }

        public async Task UnDeleteHouse(int Id)
        {
            if (HoseExists(Id))
            {
                var House = await GetHouseById(Id);
                House.IsDeleted = false;
                await UpdateHouse(House);

            }
        }
    }
}
