﻿using DAL;
using DAL.Models;
using Microsoft.EntityFrameworkCore;
using Service.RepositoryService.Intefaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryService.Repositories
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly ApplicationDbContext _Db;

        public OwnerRepository(ApplicationDbContext db)
        {
            _Db = db;
        }

        public async Task<List<Tb_Owner>> GetAllOwners()
        {
            return await _Db.tb_Owners.ToListAsync();
        }

        public async Task<Tb_Owner> GetOwnerByphone(string phone)
        {
            return await _Db.tb_Owners.Where(q => q.Phone == phone).FirstOrDefaultAsync();
        }

        public async Task<List<Tb_House>> GetOwnerHouses(int Id)
        {
            return await _Db.tb_Houses.Where(q => q.OwnerId == Id & q.IsDeleted==false).ToListAsync();
        }

        public async Task<bool> OwnerExists(string phone)
        {
            return await _Db.tb_Owners.AnyAsync(q => q.Phone == phone);
        }
    }
}
