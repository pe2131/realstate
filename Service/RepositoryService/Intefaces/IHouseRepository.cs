﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryService.Intefaces
{
   public interface IHouseRepository
    {
        Task<List<Tb_House>> GetAllHouses();
        Task<List<Tb_House>> GetDeletedHouses();
        Task InsertHouse(Tb_House Model);
        Task<Tb_House> GetHouseById(int id);

        Task UpdateHouse(Tb_House Model);
        Task DeleteHouse(Tb_House Model);
        Task DeleteHouse(int Id);
        Task UnDeleteHouse(int Id);
        bool HoseExists(int Id);
    }
}
