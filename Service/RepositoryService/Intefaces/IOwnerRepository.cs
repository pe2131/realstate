﻿using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Service.RepositoryService.Intefaces
{
   public interface IOwnerRepository
    {
        Task<List<Tb_Owner>> GetAllOwners();
        Task<bool> OwnerExists(string phone);
        Task<Tb_Owner> GetOwnerByphone(string phone);
        Task<List<Tb_House>> GetOwnerHouses(int Id);

    }
}
