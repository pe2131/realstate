﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Service.RepositoryService.Intefaces;

namespace RealState.Controllers
{
    public class OwnerController : Controller
    {
        private readonly IOwnerRepository _ownerRepository;

        public OwnerController(IOwnerRepository ownerRepository)
        {
            _ownerRepository = ownerRepository;
        }

        public async Task<IActionResult> Index()
        {

            return View(await _ownerRepository.GetAllOwners());
        }
        public async Task<IActionResult> ShowOwnersHouse(int id)
        {

            return View(await _ownerRepository.GetOwnerHouses(id));
        }
    }
}
