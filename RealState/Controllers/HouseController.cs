﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DAL.Models;
using Microsoft.AspNetCore.Mvc;
using Service.RepositoryService.Intefaces;

namespace RealState.Controllers
{
    public class HouseController : Controller
    {
        private readonly IHouseRepository _houseRepository;
        private readonly IOwnerRepository _ownerRepository;


        public HouseController(IHouseRepository houseRepository, IOwnerRepository ownerRepository)
        {
            _houseRepository = houseRepository;
            _ownerRepository = ownerRepository;
        }

        public async Task<IActionResult> Index()
        {
            try
            {
                return View(await _houseRepository.GetAllHouses());

            }
            catch (Exception)
            {

                throw;
            }
        }
        public IActionResult Create()
        {
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Create(Tb_House model,string phone)
        {
            if (await _ownerRepository.OwnerExists(phone))
            {
                var owner = await _ownerRepository.GetOwnerByphone(phone);
                if (ModelState.IsValid)
                {
                    Tb_House house = new Tb_House
                    {
                        Address = model.Address,
                        EditAt = DateTime.Now,
                        CreateAt = DateTime.Now,
                        IsDeleted = false,
                        Meters = model.Meters,
                        Name = model.Name,
                        Type = model.Type,
                        OwnerId=owner.Id
                    };
                    await _houseRepository.InsertHouse(house);
                    return RedirectToAction(nameof(Index));
                }
                
            }
            else
            {
                ModelState.AddModelError(string.Empty, "Phon number didnt exist in owners");
            }
            return View();
        }
        public async Task<IActionResult> EditAsync(int id)
        {
            var house = await _houseRepository.GetHouseById(id);
            return View(house);
        }
        [HttpPost]
        public async Task<IActionResult> Edit(Tb_House model, string phone)
        {

            if (await _ownerRepository.OwnerExists(phone))
            {
                var owner = await _ownerRepository.GetOwnerByphone(phone);
                if (ModelState.IsValid)
                {
                    var house = await _houseRepository.GetHouseById(model.Id);
                    house.Address = model.Address;
                    house.EditAt = DateTime.Now;
                    house.Meters = model.Meters;
                    house.Name = model.Name;
                    house.OwnerId = owner.Id;
                    house.Type = model.Type;
                    await _houseRepository.UpdateHouse(house);
                    return RedirectToAction(nameof(Index));

                }

            }
            else
            {
                ModelState.AddModelError(string.Empty, "Phon number didnt exist in owners");
            }
            return View();
        }
        public async Task<IActionResult> Delete(int id)
        {
            await _houseRepository.DeleteHouse(id);
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> UnDelete(int id)
        {
            await _houseRepository.UnDeleteHouse(id);
            return RedirectToAction(nameof(Index));
        }
        public async Task<IActionResult> Deleted()
        {
            return View(await _houseRepository.GetDeletedHouses());
        }
    }
}
