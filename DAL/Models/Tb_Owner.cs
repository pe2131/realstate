﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace DAL.Models
{
  public  class Tb_Owner
    {
        [Key]
        public int Id { get; set; }
        public string Name  { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }


        #region Relations
        public ICollection<Tb_House> tb_Houses { get; set; }
        #endregion

    }
}
