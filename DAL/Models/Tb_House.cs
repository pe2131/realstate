﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace DAL.Models
{
    public class Tb_House
    {
        [Key]
        public int Id { get; set; }
        [Display(Name = "House Name")]
        [Required]
        public string Name { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public float Meters { get; set; }
        public HouseType Type { get; set; }
        public DateTime CreateAt { get; set; }
        public DateTime EditAt { get; set; }
        public bool IsDeleted { get; set; }
        public int OwnerId { get; set; }


        #region Relation
        [ForeignKey(nameof(OwnerId))]
        public Tb_Owner Tb_Owner { get; set; }
        #endregion

    }
    public enum HouseType
    {
        IsNorth = 1,
        IsSouth = 2
    }

}
