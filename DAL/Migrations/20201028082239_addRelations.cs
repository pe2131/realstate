﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class addRelations : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tb_Houses_tb_Owners_Tb_OwnerId",
                table: "tb_Houses");

            migrationBuilder.DropIndex(
                name: "IX_tb_Houses_Tb_OwnerId",
                table: "tb_Houses");

            migrationBuilder.DropColumn(
                name: "Tb_OwnerId",
                table: "tb_Houses");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "tb_Houses",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "tb_Houses",
                nullable: false,
                oldClrType: typeof(string),
                oldType: "nvarchar(max)",
                oldNullable: true);

            migrationBuilder.AddColumn<DateTime>(
                name: "CreateAt",
                table: "tb_Houses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<DateTime>(
                name: "EditAt",
                table: "tb_Houses",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));

            migrationBuilder.AddColumn<bool>(
                name: "IsDeleted",
                table: "tb_Houses",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<int>(
                name: "OwnerId",
                table: "tb_Houses",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.InsertData(
                table: "tb_Owners",
                columns: new[] { "Id", "LastName", "Name", "Phone" },
                values: new object[] { 1, "Shakespeare", "William", "0980000123" });

            migrationBuilder.InsertData(
                table: "tb_Owners",
                columns: new[] { "Id", "LastName", "Name", "Phone" },
                values: new object[] { 2, "Doe", "John", "098000000124" });

            migrationBuilder.CreateIndex(
                name: "IX_tb_Houses_OwnerId",
                table: "tb_Houses",
                column: "OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_tb_Houses_tb_Owners_OwnerId",
                table: "tb_Houses",
                column: "OwnerId",
                principalTable: "tb_Owners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_tb_Houses_tb_Owners_OwnerId",
                table: "tb_Houses");

            migrationBuilder.DropIndex(
                name: "IX_tb_Houses_OwnerId",
                table: "tb_Houses");

            migrationBuilder.DeleteData(
                table: "tb_Owners",
                keyColumn: "Id",
                keyValue: 1);

            migrationBuilder.DeleteData(
                table: "tb_Owners",
                keyColumn: "Id",
                keyValue: 2);

            migrationBuilder.DropColumn(
                name: "CreateAt",
                table: "tb_Houses");

            migrationBuilder.DropColumn(
                name: "EditAt",
                table: "tb_Houses");

            migrationBuilder.DropColumn(
                name: "IsDeleted",
                table: "tb_Houses");

            migrationBuilder.DropColumn(
                name: "OwnerId",
                table: "tb_Houses");

            migrationBuilder.AlterColumn<string>(
                name: "Name",
                table: "tb_Houses",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AlterColumn<string>(
                name: "Address",
                table: "tb_Houses",
                type: "nvarchar(max)",
                nullable: true,
                oldClrType: typeof(string));

            migrationBuilder.AddColumn<int>(
                name: "Tb_OwnerId",
                table: "tb_Houses",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_tb_Houses_Tb_OwnerId",
                table: "tb_Houses",
                column: "Tb_OwnerId");

            migrationBuilder.AddForeignKey(
                name: "FK_tb_Houses_tb_Owners_Tb_OwnerId",
                table: "tb_Houses",
                column: "Tb_OwnerId",
                principalTable: "tb_Owners",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
