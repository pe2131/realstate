﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace DAL.Migrations
{
    public partial class Initial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "tb_Owners",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    LastName = table.Column<string>(nullable: true),
                    Phone = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_Owners", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "tb_Houses",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Address = table.Column<string>(nullable: true),
                    Meters = table.Column<float>(nullable: false),
                    Type = table.Column<int>(nullable: false),
                    Tb_OwnerId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_tb_Houses", x => x.Id);
                    table.ForeignKey(
                        name: "FK_tb_Houses_tb_Owners_Tb_OwnerId",
                        column: x => x.Tb_OwnerId,
                        principalTable: "tb_Owners",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_tb_Houses_Tb_OwnerId",
                table: "tb_Houses",
                column: "Tb_OwnerId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "tb_Houses");

            migrationBuilder.DropTable(
                name: "tb_Owners");
        }
    }
}
