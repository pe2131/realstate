﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL
{
   public class ApplicationDbContext:DbContext
    {

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options):base(options)
        {

        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Tb_Owner>().HasData(
                new Tb_Owner
                {
                    Id = 1,
                    Name = "William",
                    LastName = "Shakespeare",
                    Phone="0980000123"
                },
                new Tb_Owner
                {
                    Id=2,
                    Name="John",
                    LastName="Doe",
                    Phone="098000000124"
                }
            );
        }
        public DbSet<Tb_Owner> tb_Owners { get; set; }
        public DbSet<Tb_House> tb_Houses { get; set; }

    }
}
